# Keyboard demo / proof-of-concept for the flow3r badge

This is a work-in-progress demo for potential keyboard input for the flow3r badge. I hope to get this to a point where it can be bundled with the badges firmware.

## License
All original source code in this repository is Copyright (C) 2023 Flow3r Badge Contributors. This source code is licensed under the GNU Lesser General Public License Version 3.0 as described in the file COPYING.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License Version 3.0 as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License Version 3.0 along with this program.  If not, see <https://www.gnu.org/licenses/>.
